import subprocess

import os
import click
import tqdm

import glob

@click.command()
@click.argument("in_dir", type=click.Path())
@click.argument("out_dir", type=click.Path())
def run(
    in_dir: str,
    out_dir: str
):
    """Gets all png from input directory and uses black-preproc on them
    :param in_dir: 
    :param out_dir:
    :return:
    """
    
    inputs = glob.glob(os.path.join(in_dir,'**/*.png'), recursive=True)
    
    for in_path in tqdm.tqdm(inputs):
        out_path = os.path.join(out_dir, os.path.relpath(in_path, in_dir))
        subprocess.run(["python", "-m", "src.ops.blackground", in_path, out_path])
    

if __name__ == "__main__":
    run()
