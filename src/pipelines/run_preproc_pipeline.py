import os
import subprocess


for i in ['01', '02', '03']:
    subprocess.run(['python', '-m' 'src.pipelines.mass_blackground', f'data/raw/Aztek/{i}', f'data/inter/black/Aztek/{i}'])

for src in ['bboxes', 'masks']:
    subprocess.run(['python', '-m' 'src.ops.rename_data', f'data/hand/{src}/Aztek', '(.*)', '\\g<1>', '-o', f'data/inter/{src}/Aztek', '-c'])

for src in ['black', 'bboxes', 'masks']:
    subprocess.run(['python', '-m' 'src.ops.rename_data', f'data/inter/{src}/Aztek/01', "JLA Presents- Aztek - The Ultimate Man-", "Aztek_01_"])
    subprocess.run(['python', '-m' 'src.ops.rename_data', f'data/inter/{src}/Aztek/02', "Aztek The Ultimate Man #2 ", "Aztek_02_"])
    subprocess.run(['python', '-m' 'src.ops.rename_data', f'data/inter/{src}/Aztek/03', "Aztek #3 ", "Aztek_03_"])
    
    subprocess.run(['python', '-m' 'src.ops.rename_data', f'data/inter/{src}/Aztek', 'layer_0', 'data', '-o', 'data/ready2/Aztek/', '-c'])
    subprocess.run(['python', '-m' 'src.ops.rename_data', f'data/inter/{src}/Aztek', 'layer_[1-9]+[0-9]*', 'mask', '-o', 'data/ready2/Aztek/', '-c'])
 
subprocess.run(['python', '-m' 'src.pipelines.mass_read_bbox', f'data/inter/bboxes/Aztek/', '-o', f'data/ready2/Aztek/', '-f', 'xywh'])

