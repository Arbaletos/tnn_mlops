import subprocess
import json

original_file = "data/ready2/Aztek/01/Aztek_01_006_data.png"

subprocess.run(["python", "-m", "src.normalizer", original_file, "Aztek_image.png"])

subprocess.run(["python", "-m", "src.segmenter", "Aztek_image.png", "Aztek_mask.png",
                "-s", original_file.replace("data.png", "mask.png")])

subprocess.run(["python", "-m", "src.detecter", "Aztek_mask.png", "Aztek_bbox.json",
                "-s", original_file.replace("data.png", "bub.json")])

subprocess.run(
    ["python", "-m", "src.numerizer", "Aztek_bbox.json", "Aztek_bbox_ord.json", "-m", "copy"]
)

subprocess.run(
    ["python", "-m", "src.cleaner", "Aztek_image.png", "Aztek_mask.png", "Aztek_clean.png", "-m", "white_fill"]
)

# Creating stub txt file
bubbles = json.loads(open("Aztek_bbox_ord.json", encoding="utf-8").read())
with open("Aztek_text.txt", "w", encoding="utf-8") as txt:
    txt.write('@font_size:25\n@interval:5\n')
    
    txt.write("LOREM IPSUM DOLOR SIT AMET\n\n" * len(bubbles))

subprocess.run(["python", "-m", "src.formater", "Aztek_text.txt", "Aztek_bbox_ord.json",
                "Aztek_mask.png", "Aztek_text_fmt.txt", "-m", "box_align"])
                
subprocess.run(["python", "-m", "src.typer", "Aztek_text_fmt.txt", "Aztek_clean.png",
                "Aztek_rezult.png"])
