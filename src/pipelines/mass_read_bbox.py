import subprocess

import os
import click
import tqdm

import glob

@click.command()
@click.argument("in_dir", type=click.Path())
@click.option("-o", "--out_dir", type=click.Path(), default = None)
@click.option("-f", "--format", default='xywh')
def run(
    in_dir: str,
    out_dir: str,
    format: str
):
    """Gets all png from input directory and saves bboxes of them
    :param in_dir: 
    :param out_dir:
    :return:
    """
    if out_dir is None:
        out_dir = in_dir
        
    inputs = glob.glob(os.path.join(in_dir,'**/*.png'), recursive=True)
    
    for in_path in tqdm.tqdm(inputs):
        out_path = os.path.join(out_dir, os.path.relpath(in_path, in_dir)).replace('.png', '.json')
        subprocess.run(["python", "-m", "src.ops.read_bbox", in_path, out_path, '-f', format])
    

if __name__ == "__main__":
    run()
