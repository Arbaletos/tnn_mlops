import os

import click

from src.click_utils import prompt_copy, ext_assert
from src.data.image_utils import load_image, save_image


@click.command()
@click.argument("in_path", type=click.Path(exists=True))
@click.argument("out_path", type=click.Path())
def normalize_image(in_path: str, out_path: str):
    """Normalizes non-png images to png. In stub-mode returns the same image for png.
    :param in_path: Path to read input image
    :param out_path: Path to save normalized (or same for png) image
    :return:
    """

    if in_path.lower() == out_path.lower():
        print("Input/Output image is the same!")
        return

    ext_assert(out_path, ".png")
    try:
        img = load_image(in_path)
        save_image(out_path, img)
    except:
        print('Input format is not recognised!')


if __name__ == "__main__":
    normalize_image()
