import os
import shutil


def prompt_copy(in_path: str, out_path: str):
    """Copies in_path to out_path, prompts overwrite if out_path exists.
    :param in_path source file
    :param out_path target file
    :return:
    """
    assert in_path != out_path, "Can't overwrite the same path!"
    assert os.path.exists(in_path), "Input path is not exists!"
    if os.path.exists(out_path):
        ow = input("Ouput path already exists! Overwrite? (y/n)")
        if ow != "y":
            print("Finishing...")
            return
        os.remove(out_path)
    shutil.copy(in_path, out_path)


def ext_assert(path, ext):
    """ Asserts, that path has extension ext. Lowercases input path!
    :param path
    :param ext
    """
    assert path.lower().endswith(ext), f"Extension mismatch for {path}, {ext}!"
