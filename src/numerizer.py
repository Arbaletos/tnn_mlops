import click

from src.click_utils import ext_assert, prompt_copy


@click.command()
@click.argument("in_path", type=click.Path(exists=True))
@click.argument("out_path", type=click.Path())
@click.option("-i", "--src_image", default="", type=click.Path(), help="source image")
@click.option("-m", "--method", default="copy", help="method")
def numerate(in_path: str, out_path: str, src_image: str = "", method: str = "stub"):
    """Enumerates bubble_boxes in order, corresponding to optional input image
    and manga/comics mode
    :param in_path: Input json with bubble-boxes
    :param out_path: Output json with bubble-boxes
    :param src_path: source image, optional to run some image-dependent ranking
    :param method: method for numerizing (e.g. copy)
    :return:
    """
    ext_assert(in_path, ".json")
    ext_assert(out_path, ".json")

    if method == "copy":
        if in_path != out_path:
            prompt_copy(in_path, out_path)
    else:
        print("Not Implemented!")


if __name__ == "__main__":
    numerate()
