import os

import click

from src.click_utils import prompt_copy, ext_assert
from src.markup.txtfmt import read_lines, parse_lines, validate_lines

from PIL import Image, ImageDraw, ImageFont


def run_type(draw, bub, x, y, font, interval, font_size, **kwargs):
    font = ImageFont.truetype(font, font_size)
    
    h = font.getsize(bub[0][0])[1]
    for i, (line, dx) in enumerate(bub):
        draw.text((x+dx, y+(h+interval)*i), line, font=font, fill=(0, 0, 0))

@click.command()
@click.argument("in_txt_path", type=click.Path(exists=True))
@click.argument("in_img_path", type=click.Path(exists=True))
@click.argument("out_path", type=click.Path())
@click.option("-m", "--method", default="box", help="method")
@click.option(
    "-s", "--stub_path", default="", type=click.Path(), help="stub_formated_txt"
)
def type_image(
    in_txt_path: str,
    in_img_path: str,
    out_path: str,
    method: str = "",
    stub_path: str = "",
):
    """Uses formatted txt to type input cleaned image
    :param in_txt_path: input formatted txt file
    :param in_img_path: input cleaned image to type on
    :param out_path: Path to save result image
    :param method: Method of typing (box)
    :param stub_path: stub path with target image
    :return:
    """

    ext_assert(in_txt_path, ".txt")
    ext_assert(in_img_path, ".png")
    ext_assert(out_path, ".png")

    if stub_path:
        if not os.path.exists(stub_path):
            print("Stub image is not found, autoresolving...")
            stub_path = in_img_path.replace("clean.png", "data.png")
            if not os.path.exists(stub_path) or not stub_path.endswith("data.png"):
                print("Malsukcese!")
                return
            else:
                print(f"Stub image is: {stub_path}")
        ext_assert(stub_path, ".png")
        prompt_copy(stub_path, out_path)
        return
    
    lines = parse_lines(read_lines(in_txt_path))
    err = validate_lines(lines)
    if err:
        print('Errors while reading formatted txt!')
        print(err)
        return
    img = Image.open(in_img_path)
    draw = ImageDraw.Draw(img)
    
    for ctx, bub in lines:
        run_type(draw, bub, **ctx)
    img.save(out_path)

if __name__ == "__main__":
    type_image()
