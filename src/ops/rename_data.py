import subprocess

import os
import click
import tqdm

import shutil
import re

import glob

def rename_data(in_dir, reg_in, reg_out, out_dir=None, copy=False):
    if not out_dir:
        out_dir = in_dir
    inputs = [os.path.relpath(g, in_dir) for g in glob.glob(os.path.join(in_dir, '**/*.png'), recursive=True)]
    
    pattern = re.compile(reg_in)
    for file in tqdm.tqdm(inputs):
        if pattern.findall(file):
            new_name = pattern.sub(reg_out, file)
            if new_name != file or in_dir != out_dir:
                target_file = os.path.join(out_dir, new_name)
                os.makedirs(os.path.dirname(target_file), exist_ok=True)
                if os.path.exists(target_file):
                    os.remove(target_file)
                if copy:
                    shutil.copy(os.path.join(in_dir, file), target_file)
                else:
                    os.rename(os.path.join(in_dir, file), target_file)

@click.command()
@click.argument("in_dir", type=click.Path())
@click.argument("in_regex")
@click.argument("out_regex")
@click.option("-o", "--out_dir", type=click.Path(), default='')
@click.option("-c", "--copy", is_flag=True, default=False)
def run(
    in_dir: str,
    in_regex: str,
    out_regex: str,
    out_dir: str,
    copy: bool,
):
    rename_data(in_dir, in_regex, out_regex, out_dir, copy)

      
if __name__ == "__main__":
    run()
