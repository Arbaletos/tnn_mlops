import os

import click
import imageio
import numpy as np

from src.click_utils import prompt_copy, ext_assert

def blackground(in_path, out_path):
    img = imageio.imread(in_path)
    #Image shape is HxWx4 uint8
    
    if img[:,:,-1].mean() < 255:
        img[img[:,:,-1] < 255] = 0
        img[:,:,-1] = 255
    """
    ind = img[:,:,-1] < 255
    ind = ind.reshape(ind.shape[0], ind.shape[1], 1)
    ind = np.concatenate([ind]*4,axis=2)
    ind[:,:,-1] = False
    img[ind] = 0
    img[:,:,-1] = 255
    """
    
    os.makedirs(os.path.dirname(out_path), exist_ok=True)
    imageio.imsave(out_path, img) 



@click.command()
@click.argument("in_img_path", type=click.Path(exists=True))
@click.argument("out_img_path", type=click.Path())
def run(
    in_img_path: str,
    out_img_path: str
):
    """Adds black background instead of alpha channel of image
    :param in_img_path: Path to read input image
    :param out_img_path: Path to read input image
    :return:
    """

    ext_assert(in_img_path, ".png")
    ext_assert(out_img_path, ".png")
    blackground(in_img_path, out_img_path)
    

if __name__ == "__main__":
    run()
