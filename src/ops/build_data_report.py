import os
import pandas as pd
import plotly.express as px
import re
import glob 
import datetime

def calc_distinct(listo):
    return len(set([re.sub('(_layer_[0-9]+|_mask|_data|_bub)', '', l) for l in listo]))

def run():
    currentDateTime = datetime.datetime.now()
    date = currentDateTime.date().strftime("%Y-%m-%d")

    total_raw = calc_distinct(glob.glob('data/raw/**/*.png', recursive = True))
    hand_seg = set([os.path.dirname(g).replace('hand/masks', 'raw') for g in glob.glob('data/hand/masks/**/*.png', recursive = True)])
    raw_corrected = calc_distinct([g for g in glob.glob('data/raw/**/*.png', recursive = True) if any([hs in g for hs in hand_seg])])
    raw_bboxes = calc_distinct(glob.glob('data/hand/bboxes/**/*.png', recursive = True))
    mask_data_pairs = calc_distinct([g for g in glob.glob('data/ready/**/*_data.png', recursive=True) if os.path.exists(g.replace('_data.png', '_mask.png'))])
    mask_bbox_pairs = calc_distinct([g for g in glob.glob('data/ready/**/*_mask.png', recursive=True) if os.path.exists(g.replace('_mask.png', '_bub.json'))])

    df = pd.DataFrame({'Category':['Total raw images', 'Total hand-checked images',
                                   'Total bbox-segmented images', 'Total Data-Mask pairs',
                                   'Total Mask-Bbox pairs'][::-1],
                   'Amount': [total_raw, raw_corrected, raw_bboxes,
                              mask_data_pairs, mask_bbox_pairs][::-1]})
                   
    fig = px.bar(data_frame=df, x='Amount', y='Category', title=f'Data Amount Report {date}', text='Amount')
    
    report_dir = f'reports/data_reports/'
    fig.write_html(os.path.join(report_dir, date+'.html'), include_plotlyjs='cdn', full_html=True)
    print('Finish!')
    
if __name__ == "__main__":
    run()
