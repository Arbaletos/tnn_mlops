import subprocess

import os
import click
import tqdm

import shutil
import re

import glob


@click.command()
@click.argument("in_dir", type=click.Path())
@click.argument("out_dir", type=click.Path())
def run(
    in_dir: str,
    out_dir: str
):
    inputs = glob.glob(os.path.join(in_dir,'**/*.png'), recursive=True)
    inputs += glob.glob(os.path.join(in_dir,'/*.png'))
    
    for in_path in tqdm.tqdm(inputs):
        rel_path = os.path.relpath(in_path, in_dir)
        if 'layer_0' in in_path:
            rel_path = rel_path.replace('layer_0', 'data')
        elif re.findall('layer_[0-9]+.png', rel_path):
            rel_path = re.sub('layer_[0-9]+.png', 'mask.png', rel_path) 
        else:
            continue
        out_path = os.path.join(out_dir, rel_path)
        os.makedirs(os.path.dirname(out_path), exist_ok=True)
        shutil.copy(in_path, out_path)
      
if __name__ == "__main__":
    run()
