import os
import click

import json 

from src.markup.bbox import read_bub, xywh_to_xyxy
from src.click_utils import ext_assert

def read_bbox(in_path, out_path, format='xywh'):
    assert format in ['xywh', 'xyxy'], "Unrecognized bbox format!"

    rects = read_bub(in_path)
    if format == 'xywh':
        js = json.dumps([{a: b for a, b in zip(format, r)} for r in rects])
    else:
        js = json.dumps([{a: b for a, b in zip(['x1', 'y1', 'x2', 'y2'], xywh_to_xyxy(r))} for r in rects])
    os.makedirs(os.path.dirname(out_path), exist_ok=True)
    open(out_path, 'w', encoding='utf-8').write(js)


@click.command()
@click.argument("in_path", type=click.Path())
@click.argument("out_path", type=click.Path())
@click.option("-f", "--format", default='xywh')
def run(
    in_path: str,
    out_path: str,
    format: str,
):
    ext_assert(in_path, '.png')
    ext_assert(out_path, '.json')
    read_bbox(in_path, out_path, format)

      
if __name__ == "__main__":
    run()
