import imageio
import numpy as np

def save_image(path, img):
    img = (img*255).astype(np.uint8)
    imageio.imsave(path, img) 

def load_image(path):
    return imageio.imread(path)[:,:,:3].astype(np.float32)/255


def load_mask(path):
    return imageio.imread(path)[:,:,0].astype(np.float32)/255
    
    
def split_image(image, size=512, hop=512):
    ret = []
    h, w = image.shape[:2]
    y, x = 0, 0
    while x < w and y < h:
        curimage = np.zeros([size,size]+list(image.shape[2:]), dtype=np.float32)
        curimage[:min(size, h-y), :min(size, w-x)] = image[y:y+size,x:x+size]
        ret.append(curimage)
        x+=hop
        if x>=w:
            x = 0
            y += hop
    return ret


def combine_image(pieces, hop, h, w):
    image = np.zeros([h,w]+list(pieces[0].shape[2:]), dtype=np.float32)
    cx = 0
    cy = 0
    for p in pieces:
        image[cy:cy+p.shape[1],cx:cx+p.shape[0]] = p[:h-cy,:w-cx]
        cx += hop
        if cx >= w:
            cx = 0
            cy += hop
    return image
    
