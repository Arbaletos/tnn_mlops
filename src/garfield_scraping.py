from pathlib import Path
from PIL import Image
from typing import List
import logging
import click
import requests
import re


class GarfieldScraper:
    def __init__(self, first_num: int, ru_flag: bool, save_dir: Path):
        self.first_num: int = first_num
        self.ru_flag: bool = ru_flag
        self.save_dir: Path = save_dir

    def __get_orig_transl(self, page_id: int, urls: List[str]):
        urls = sorted(urls)
        logging.info(f"{page_id} - urls: {urls}")

        if self.ru_flag:
            response = requests.get(urls[1])
            if response.status_code != 404:
                with open(self.save_dir / f"{page_id}_ru.png", "wb") as file:
                    file.write(response.content)
            else:
                return None

        response = requests.get(urls[0])
        with open(self.save_dir / f"{page_id}_en.png", "wb") as file:
            file.write(response.content)

    def __get_garfield_imgs(self):
        num = self.first_num

        while True:
            url = f"https://garfield-archive.ru/num{num}"
            r = requests.get(url, allow_redirects=True)
            result = re.findall(r"url\((\/[\/a-z0-9\-.]+)\)", str(r.content))
            img_urls = [
                f"https://garfield-archive.ru{address}"
                for address in list(set(result))
                if ".gif" not in address
            ]

            if len(img_urls) > 0:
                self.__get_orig_transl(num, img_urls)
            else:
                break

            num += 1

    def __crop_ru_imgs(self):
        for img_path in list(self.save_dir.glob("*_ru.png")):
            with Image.open(img_path) as im:
                width, height = im.size
                im = im.crop((0, 30, width, height - 25))
                im.save(img_path)
                logging.info(f"cropped: {img_path} - {im.size}")

    def scraping(self):
        self.__get_garfield_imgs()
        self.__crop_ru_imgs()


@click.command()
@click.option(
    "-f",
    "--first_num",
    type=int,
    default=1,
    help="select first edition"
)
@click.option(
    "-r",
    "--ru_flag",
    type=bool,
    default=True,
    help="whether translated pages should be downloaded",
)
@click.option(
    "-d",
    "--save_dir",
    type=Path,
    default=Path("../data/raw/garfield"),
    help="directory for saving scans",
)
def scraping_garfield(
    first_num: int = 1, ru_flag: bool = True, save_dir: Path = Path("../data/raw/garfield")
):
    logging.basicConfig(
        format="%(asctime)s %(message)s",
        datefmt="%m/%d/%Y %I:%M:%S %p",
        encoding="utf-8",
        level=logging.INFO,
    )
    scraper = GarfieldScraper(first_num, ru_flag, save_dir)
    scraper.scraping()


if __name__ == "__main__":
    scraping_garfield()
