import os

import json
import itertools

import click
import numpy as np
import imageio 
from PIL import Image, ImageDraw, ImageFont

from src.click_utils import prompt_copy, ext_assert
from src.markup.txtfmt import read_lines, parse_lines, dump_lines

import matplotlib.pyplot as plt


def score_mask(variant, ctx, box, pos_mask, neg_mask):
    font = ImageFont.truetype(ctx['font'], ctx['font_size'])
    
    zero_mask = np.zeros(pos_mask.shape, dtype=np.int16)
    pos = np.zeros(pos_mask.shape, dtype=np.float32)
    neg = neg_mask.copy()
 
    dy = ctx['y'] - box['y']
    sint_int = int(np.ceil(ctx['interval'] / 2))
    

    for i0, (string, dx) in enumerate(variant):
        y_before = sint_int
        y_after = sint_int
        if i0 == 0:
            y_before = 0
        if i0 == len(string)-1:
            y_after = 0
        sx, sy = font.getsize(string)
        
        zero_mask[dx:dx+sx, dy-y_before:dy+sy+y_after,] = 1
        dy += ctx['font_size'] + ctx['interval']

    neg[zero_mask==1] = 0
    pos[zero_mask==1] = pos_mask[zero_mask == 1]
    return pos.sum() + neg.sum()


def get_max_r(dx, dy, x0, y0, mask):

    x_max = mask.shape[0]
    y_max = mask.shape[1]

    cx = float(x0)
    cy = float(y0)
    r = 0
    #print(mask.shape)
    #print(x_max, y_max, cx, cy)
    while True:
        if mask[int(cx), int(cy)]==0:
            break
        cx += dx
        cy += dy
        if cx >= x_max or cx < 0:
            break
        if cy >= y_max or cy < 0:
            break
        r = ((cx - x0)**2 + (cy - y0)**2)**(1/2)
    return r
    
    
def get_dx_dy(tx, ty, cx, cy, prec=2):
    dx = tx - cx
    dy = ty - cy
    m = max(abs(dx), abs(dy))
    dx /= m
    dy /= m
    
    return round(dx,prec), round(dy,prec)
    
    
def prepare_r_dict(mask, cx, cy):
    r_dict = {}
    yr = list(range(mask.shape[1]))
    xr = list(range(mask.shape[0]))

    y_lists = yr + yr + [0]*len(xr) + [mask.shape[1]]*len(xr)
    x_lists = [0]*len(yr) + [mask.shape[0]]*len(yr) + xr + xr
    
    for tx, ty in zip(x_lists, y_lists):

        dx, dy = get_dx_dy(tx, ty, cx, cy, prec=2)
        r = get_max_r(dx, dy, cx, cy, mask)
        r_dict[(dx, dy)] = r
    return r_dict
    

def get_pos_neg_masks(mask, cx, cy, r_dict=None, ring_d=20):
    if r_dict == None:
        r_dict = {}
        
    positive_mask = mask.copy().astype(np.float32) / 255
    negative_mask = mask.copy().astype(np.float32) / 255

    for y1 in range(mask.shape[1]):
        for x1 in range(mask.shape[0]):
            if x1 == cx and y1 == cy:
                continue

            dx, dy = get_dx_dy(x1, y1, cx, cy, prec=2)
            cur_r = ((x1-cx)**2 + (y1-cy)**2)**(1/2)
            try:
                max_r = r_dict[(dx, dy)]
            except:
                max_r = get_max_r(dx, dy, cx, cy, mask)
                r_dict[(dx, dy)] = max_r

            if cur_r > max_r:
                negative_mask[x1, y1] = 0.
                positive_mask[x1, y1] = 1.
                continue
            if max_r-cur_r <= ring_d:
                positive_mask[x1, y1] = 1.0 - ((max_r - cur_r)/ring_d)**2
                negative_mask[x1, y1] = 0.
            else:
                negative_mask[x1, y1] = 1.0 - (cur_r / (max_r-ring_d))**2
                positive_mask[x1, y1] = 0.0
    return positive_mask, negative_mask
    

def get_masks(mask, x, y, w, h, padding=10):
    bub_mask = mask[x:x+w,y:y+h]
    b_mask = bub_mask[:, :, 0]
    cx = b_mask.shape[0]//2
    cy = b_mask.shape[1]//2

    r_dict = prepare_r_dict(b_mask, cx, cy)

    pos, neg = get_pos_neg_masks(b_mask, cx, cy, r_dict=r_dict, ring_d=padding)
    return pos, neg
    

def onestring_align(ctx, txt, box):
    """ Sets x,y, from original bubble for each bubble, sets dx=0 for each string
    """
    ctx['x'] = ctx.get('x', box['x'])
    ctx['y'] = ctx.get('y', box['y'])
    
    ntxt = []
    for t, dx in txt:
        if dx < 0:
            ntxt.append((t, 0))
            continue
        ntxt.append((t, dx))
    return ctx, ntxt
    
    
def box_align(variant, ctx, box, padding=10):
    """
    Sets dx and dy for each line in bubble according to bbox
    """
    ret = []
    n_strings = len(variant)
    height = ctx['font_size'] * n_strings + ctx['interval'] * (n_strings-1)
 
    dy = (box['h'] - height) // 2
    font = ImageFont.truetype(ctx['font'], ctx['font_size'])
    
    for line in variant:
        w = font.getsize(line)[0]
        if ctx['align'] == 'center':
            dx = (box['w'] - w) // 2
        elif ctx['align'] == 'left':
            dx = padding
        elif ctx['align'] == 'right':
            dx = box['w'] - w - padding
        else:
            print('Incorrect align!')
            return
            
        ret.append([line, dx])
    
    ctx = ctx.copy()
    ctx['x'] = ctx.get('x', box['x'])
    ctx['y'] = ctx.get('y', box['y'])+ dy
    return ctx, ret
        
    
class Variant():
    
    def __init__(self, slist=None):
        self.var_list = []
        self.sizes = []
        self.slist = slist
    
    def add(self, grp):
        self.var_list.append(grp)
        self.sizes.append(sum([self.slist[i] for i in grp]))
        return self
    
    def copy(self):
        nvar = Variant(self.slist)
        nvar.var_list = self.var_list[:]
        nvar.sizes = self.sizes[:]
        return nvar
    
    def len_std(self):
        if len(self.sizes) <= 1:
            return 0
        ar = np.array(self.sizes)
        s = ((ar[:-1] - ar[1:])**2).mean() ** (1/2)
        return s
    
    def get_len(self):
        return len(self.var_list)
    
    def get_text(self, text_list):
        
        def translate(vlist, text_list):
            return ' '.join([text_list[i] for i in vlist])
        
        words = [translate(v, text_list) for v in self.var_list]
        return words
    
    
def split_variants_beam(text, font, max_lines=10, max_len=300, beam_size=1000):
    """
    Searcher of text split variants. Lower beam_size to make it faster.
    """
    listo = [t.strip() for t in text.split(' ')]
    space = font.getsize(' ')[0]
    sizes = [font.getsize(a)[0]+space for a in listo] 
    
    ok_list = []  # Making OK_LIST, n**2, but n is very small (<100)

    for i, s in enumerate(sizes):
        cur = s
        ok_list.append([])
        for i2, s2 in enumerate(sizes[i+1:]): #Optional BinSearch
            if cur+s2 > max_len:
                break

            ok_list[-1].append(i+i2+1)
            cur += s2  
            
    v_pool = {len(listo):[]}
    for i, _ in enumerate(sizes):
        v_pool[i] = []

    v_pool[0].append(Variant(sizes))

    for i, _ in enumerate(sizes):
        all_v = [p for p in v_pool[i] if p.get_len() < max_lines]
        all_v = sorted(all_v, key=lambda x: x.len_std())
        all_v = all_v[:beam_size]
        for v in all_v:
            v_pool[i+1].append(v.copy().add([i]))
            for o in ok_list[i]:
                v_pool[o+1].append(v.copy().add(list(range(i, o+1))))
    return [v.get_text(listo) for v in v_pool[len(listo)]]
    
    
def get_split_variants(text, box, font, font_size, interval, **kwargs):
    text, dx = text
    font = ImageFont.truetype(font, font_size)
    w = font.getsize(text)[0]
    max_h = box['h']
    max_w = box['w']
    char_w = w / len(text)
    
    max_lines = max_h // (font_size+interval)
    max_len = max_w // char_w
    
    variantes = split_variants_beam(text, font, max_lines=max_lines, max_len=max_w, beam_size=1000)
    return variantes
    
    
@click.command()
@click.argument("in_txt_path", type=click.Path(exists=True))
@click.argument("in_bbox_path", type=click.Path(exists=True))
@click.argument("in_mask_path", type=click.Path(exists=True))
@click.argument("out_path", type=click.Path())
@click.option("-m", "--method", default='onestring')
@click.option(
    "-s", "--stub_path", default="", type=click.Path(), help="stub_formated_txt"
)
def format_txt(
    in_txt_path: str,
    in_bbox_path: str,
    in_mask_path: str,
    out_path: str,
    method: str,
    stub_path: str = "",
):
    """Formates text: splits into strings, explicites coordinates of each textstring.
    :param in_txt_path: input raw file with text
    :param in_bbox_path:
    :param in_mask_path: binary bubble mask
    :param out_path: Path to save formatted txt
    :param method: Method to use (onestring, box_align, mask_align)
    :param stub_path: stub path for formatter
    :return:
    """

    ext_assert(in_txt_path, ".txt")
    ext_assert(in_bbox_path, ".json")
    ext_assert(in_mask_path, ".png")
    ext_assert(out_path, ".txt")

    if stub_path:  # Stub mode
        if not os.path.exists(stub_path):
            print("Stub txt is not found, autoresolving...")
            stub_path = in_img_path.replace(".txt", "_fmt.txt")
            if not os.path.exists(stub_path) or not stub_path.endswith("_fmt.txt"):
                print("Malsukcese!")
                return
            else:
                print(f"Stub image is: {stub_path}")
        ext_assert(stub_path, ".txt")
        prompt_copy(stub_path, out_path)
        return
        
    lines = [l.strip() for l in open(in_txt_path, encoding='utf-8')]
    fmt = parse_lines(read_lines(in_txt_path))
    bbox = json.loads(open(in_bbox_path, encoding='utf-8').read())
    mask = imageio.imread(in_mask_path).transpose((1,0,2))
    
    if method == 'onestring':
        aligned = []
        for (ctx, bub), box in zip(fmt, bbox):
            ctx, bub = onestring_align(ctx, bub, box)
            aligned.append((ctx, bub))
            
    elif method in ['box_align', 'mask_align']:
        aligned = []
        for (ctx, bub), box in zip(fmt, bbox):
            if len(bub)>1: # Do not autosplit splitted sequences!
                split_variants = [[b[0] for b in bub]]
            else:
                split_variants = []
                while not split_variants:
                    split_variants = get_split_variants(bub[0], box, **ctx)
                    if split_variants:
                        break
                    box['w']+=10
                    box['h']+=10
                    box['x']-=5
                    if box['x'] < 0:
                        box['w'] += box['x']
                        box['x'] = 0
                    box['y']-=5
                    if box['y'] < 0:
                        box['h'] += box['y']
                        box['y'] = 0
            
            box_fit_variants = [box_align(v, ctx, box) for v in split_variants]
            print(mask.shape)
            print(box)
            pos, neg = get_masks(mask, **box)
           
            if method == 'mask_align':
                print('NotImplemented')
                best = box_fit_variants[0]
            elif method == 'box_align':
                losses = [score_mask(v, c, box, pos, neg) for c, v in box_fit_variants]
                best = box_fit_variants[losses.index(min(losses))]
            else:
                print('NotImplemented!')
                return
               
                
            aligned.append(best)
             
                
    out_lines = dump_lines(aligned)
    open(out_path, 'w', encoding='utf-8').write('\n'.join(out_lines))
        

if __name__ == "__main__":
    format_txt()

