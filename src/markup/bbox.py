import itertools

import numpy as np
import imageio

from src.data.image_utils import load_image

def read_bub(in_path):
    """Reads .bub png image and saves bubble boxes as json.
    Boxes are drawn as #FFFF00 color rectangles."""

    img = load_image(in_path)

    starts = []
    mask = (img.sum(axis=2) == 2.0) * 1.0
    h = mask.shape[0]
    w = mask.shape[1]
    ret = []
    incdice_array = np.array(
        [(x, y) for x, y in itertools.product(np.arange(h - 1), np.arange(w - 1))]
    ).reshape((h - 1, w - 1, 2))

    starts = mask[:-1, :-1] + mask[1:, :-1] + mask[:-1, 1:]
    starts += 1 - np.concatenate((np.zeros((1, w - 1)), mask[:-2, :-1]), axis=0)
    starts += 1 - np.concatenate((np.zeros((h - 1, 1)), mask[:-1, :-2]), axis=1)
    for ar in incdice_array[starts == 5.0]:
        y, x = ar
        row = mask[y, x:]
        column = mask[y:, x]
        for val, grp in itertools.groupby(row):
            if val == 1.0:
                w = len(list(grp))
            else:
                w = 0
            break
        for val, grp in itertools.groupby(column):
            if val == 1.0:
                h = len(list(grp))
            else:
                h = 0
            break
        ret += [(int(i) for i in [x, y, w, h])]
    return ret
    

def xywh_to_xyxy(bbox):
    x,y,w,h = bbox
    return [x, y, x+w, y+h]