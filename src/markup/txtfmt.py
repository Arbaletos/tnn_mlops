

def read_lines(path):
    return [l.strip() for l in open(path, encoding='utf-8')]
    
def parse_lines(string, default=None):
    """Transforms string list into params/text tuple-list.
    """
    if default is None:
        default = {'font_size':20,
                   'interval':5,
                   'font':'arial.ttf',
                   'align': 'center'}
    ret = []
    cur_bub = []
    cur_ctx = default.copy()
    for line in string:
        if line.startswith('@'):
            field, val = line[1:].split(':')
            if val.isdigit():
                val = int(val)
            cur_ctx[field] = val
        elif line.startswith('#'):
            dx, text = line[1:].split('|')
            cur_bub.append((text, int(dx)))
        elif not line:
            if cur_bub:
                ret.append((cur_ctx, cur_bub))
                cur_ctx = cur_ctx.copy()
                for i in 'xy':
                    if i in cur_ctx:
                        cur_ctx.pop(i)
                cur_bub = []
        else:
            cur_bub.append((line, int(-1)))
    if cur_bub:
        ret.append((cur_ctx, cur_bub))
    return ret
    
    
def dump_lines(lines):
    """Dumps params/text tuple-list back into string list"""
    ret = []
    pre_ctx = {}
    for ctx, bub in lines:
        for k in ctx:
            if k in 'xy' or ctx[k] != pre_ctx.get(k,-1):
                ret.append(f'@{k}:{ctx[k]}')
        for txt, dx in bub:
            ret.append(txt)
            if dx >= 0:
                ret[-1] = f'#{"0"*(3-len(str(dx)))}{str(dx)}|' + ret[-1]
        ret.append('')
        pre_ctx = ctx
    return ret 
    

def validate_lines(lines):
    """Validates txt: should have x,y,font, font-size, interval for each bubble, dx for each string"""
    err = []
    fields = ['x', 'y', 'font', 'font_size', 'interval']
    for i, (ctx, bub) in enumerate(lines):
        ce = []
        for f in fields:
            if f not in ctx:
                ce.append(f'Field {f} is missing!')
        for line, dx in bub:
            if dx<0:
                ce.append(f'Incorrect dx value {dx}!')
        if ce:
            err.append(f'Bubble {i+1}: {" ".join(ce)}')
    return err
        
    