import random
import os
import functools
import glob

import numpy as np
import torch
from torch.utils.data import DataLoader
import torchvision.transforms as transforms
import tqdm

from src.models.segment_validator import SegValidator, SegInferencer
from src.data.image_utils import load_image, load_mask, split_image
from src.models.segment_model import SegmentModel
from src.models.train_utils import TorchDataset, fit

import mlflow


def load_data(files):
    data = []
    target = []
    for f in files:
        if os.path.exists(f+'_data.png') and os.path.exists(f+'_mask.png'):
            try:
                data.append(load_image(f+'_data.png'))
                target.append(load_mask(f+'_mask.png'))
            except Exception as e:
                print(f'Image load Exception! {e}, {f}')
    return data, target
    
def get_sample(data, target, preprocess, size=1024):
    x = random.randint(0,data.shape[0]-size)
    y = random.randint(0,data.shape[1]-size)
    data = preprocess(data[x:x+size,y:y+size])
    target = torch.Tensor(target[x:x+size, y:y+size]).long()
    return data, target
    
def collate(data_list, preprocess, device, size=1024):
    bs = len(data_list)
    data=[]
    target=[]
    for d, t in data_list:
        sample = get_sample(d, t, preprocess, size)
        data.append(sample[0].unsqueeze(0))
        target.append(sample[1].unsqueeze(0))
    data = torch.cat(data).float().to(device)
    target = torch.cat(target).float().to(device)
    return data, target
    
print('Loading data...')
datadir = 'data'

datadir = 'data/ready'
files = [g.replace('_data.png', '') for g in glob.glob(os.path.join(datadir,'**/*_data.png'),recursive=True)]
data, target = load_data(files)

data_size = len(data)
train_size = int(data_size*0.8)
print(f'Train_size: {train_size}, Dev size: {data_size-train_size}')

print('Calculating stats...')

cat = np.concatenate([d.reshape(-1,3) for d in tqdm.tqdm(data[:train_size])], axis=0)
m, s = cat.mean(axis=0), cat.std(axis=0)

preprocess = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize(mean=m, std=s),
])

mlflow.set_tracking_uri("sqlite:///mlflow.db")
tracking_uri = mlflow.get_tracking_uri()

mlflow.set_experiment(experiment_name = 'Segment_220614')
mlflow.set_tags({'task': 'Segmentation'})

exp_dir = 'models/Segment/220614/'
img_dir = os.path.join(exp_dir, 'images')
os.makedirs(img_dir, exist_ok=True)

torch.save(preprocess, os.path.join(exp_dir, 'preproc.pt'))
mlflow.log_artifact(os.path.join(exp_dir, 'preproc.pt'))

print('Initializing model...')
torch.manual_seed(128)
device = torch.device('cpu')

model = SegmentModel().to(device)

validator = SegValidator()

inferencer = SegInferencer(model=model, infer_images=data[-2:], log_dir=exp_dir,
                           infer_params={'preproc':preprocess, 'size':512, 'hop':512})
                           
optimizer = torch.optim.SGD(model.parameters(), lr=0.001, momentum=0.99)

scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=[100,1000,2000], gamma=0.5)

loss = torch.nn.BCELoss()

data_size = len(data)

chunk_size = 512

bs = 8
train_set = DataLoader(TorchDataset(data[:train_size], target[:train_size]),
                       batch_size=bs, shuffle=True,
                       collate_fn=functools.partial(collate, preprocess=preprocess, device=device, size=512))

dev_data = np.concatenate([split_image(d) for d in data[train_size:]])
dev_target = np.concatenate([split_image(d) for d in target[train_size:]])

dev_set = DataLoader(TorchDataset(dev_data, dev_target),
                     batch_size=bs, shuffle=False,
                     collate_fn=functools.partial(collate, preprocess=preprocess, device=device, size=512))

mlflow.log_params({'chunk_size': chunk_size,
                   'train_size':str(train_size),
                   'dev_size': str(data_size-train_size),
                   'seed': 128,
                   'lr': 0.001,
                   'batch_size': bs,
                   'scheduler': 'MultiStep(gamma=0.5, milestones=[100,1000,2000])',
                   'loss': 'BCELoss(0.5, 0.5)'
                   })
                   
#test_set = DataLoader(MNIST_Dataset(test_x, test_y), batch_size=1, collate_fn=collate)
print('Training...')
fit(model, device, train_set, dev_set, loss, optimizer, scheduler,
    validator, inferencer,
    check_name = os.path.join(exp_dir, '{}.model'),
    n_epoch = 10000, i0=0, print_step=1, val_step=100,
    infer_step=100, check_step=100)
