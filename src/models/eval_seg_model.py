import torch

from src.data.image_utils import split_image, combine_image
from src.models.train_utils import load_check
from src.models.segment_model import SegmentModel


def eval_seg_model(model, img, preproc, size=512, hop=512):
    inputs = split_image(img, size, hop)
    outputs = [model.predict(i, preproc)[0,0].cpu().numpy() for i in inputs]
    ret = combine_image(outputs, hop, img.shape[0], img.shape[1])
    return ret


def init_seg_model(check_path, preproc_path=None, device=None):
    model = SegmentModel().cpu()
    load_check(model, check_path)
    if device is not None:
        if type(device) is str:
            device = torch.device(device)
        model.to(device)
    if preproc_path:
        preproc = torch.load(preproc_path)
    return model, preproc