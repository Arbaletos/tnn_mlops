import os

import numpy as np

from src.models.metrics import compute_eer
from src.models.eval_seg_model import eval_seg_model
from src.data.image_utils import save_image

from sklearn.metrics import precision_recall_fscore_support

from mlflow import log_metric, log_artifact


class SegValidator():
    def __init__(self):
        self.hats = []
        self.targets = []
    
    def start(self):
        self.hats = []
        self.targets = []
    
    def step(self, hat, target):
        self.hats += list(hat.numpy().reshape(-1))
        self.targets += list(target.numpy().reshape(-1))
        
    def aggregate(self):
        hats = np.array(self.hats)
        targets = np.array(self.targets)
        eer, t = compute_eer(targets, hats)
        
        
        
        hats_t = (hats>t).astype(np.float)
        p, r, f, s = precision_recall_fscore_support(targets, hats_t)
        log_metric("Precision_T", p[1])
        log_metric("Recall_T", r[1])
        log_metric("F1-Score_T", f[1])
        
        hats_5 = (hats>0.5).astype(np.float)
        p, r, f, s = precision_recall_fscore_support(targets, hats_5)
        log_metric("Precision_0.5", p[1])
        log_metric("Recall_0.5", r[1])
        log_metric("F1-Score_0.5", f[1])
        
        print('EER:', eer, ', T: ', t) # Metrics Calculator
        
        log_metric("EER", eer)
        log_metric("EER Thresh", float(t))
    
    
class SegInferencer():
    def __init__(self, model=None,
                 infer_images = None,
                 log_dir = '',
                 infer_params=None):
                 
        self.model = model
        self.infer_images = infer_images
        if infer_images is None:
            self.infer_images = []
        self.log_dir = log_dir
        self.infer_params = infer_params
        if infer_params is None:
            self.infer_params = {}
        
    def inference(self, n_iter=0):
        for i, img in enumerate(self.infer_images):
            prd = eval_seg_model(self.model, img, **self.infer_params)
            img_path_mask = os.path.join(self.log_dir,f'iter_{n_iter}_image_{i}')
            save_image(img_path_mask+'_data.png', img)
            log_artifact(img_path_mask+'_data.png')
            save_image(img_path_mask+'_predict.png', prd)
            log_artifact(img_path_mask+'_predict.png')