import torch

class SegmentModel(torch.nn.Module):
    def __init__(self, model_type='unet'):
        """
        self.model is unet by default. 3 channels input, 1 channel output
        """
        super().__init__()
        if model_type=='unet':
            self.model = torch.hub.load('mateuszbuda/brain-segmentation-pytorch', 'unet',
                                        in_channels=3, out_channels=1, init_features=32, pretrained=False)
        else:
            print('Unknown model type!')
        

    def forward(self, x):
        """
        For input BS x 3 x W x H we return BS x W x H mask.
        """
        return self.model(x).squeeze(1)
        
    def predict(self, x, preprocess=None):
        with torch.no_grad():
            if preprocess:
                x = preprocess(x)
            x = x.to(next(self.model.parameters()).device) # Assuming, that all parameters of a model is on a same device 
            hat = self.model(x.unsqueeze(0))
        return hat 
