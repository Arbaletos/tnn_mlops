import os
import time
import torch
from torch.utils.data import Dataset

import mlflow

class TorchDataset(Dataset):
    def __init__(self, x_data, y_data):
        super(TorchDataset).__init__()
        self.x_data = x_data
        self.y_data = y_data
    
    def __getitem__(self, index):
        return self.x_data[index], self.y_data[index]
  
    def __len__(self):
        return len(self.x_data)


def load_check(model, path):
    check = torch.load(path)['state_dict']
    model.load_state_dict(check)
    return model

    
def save_check(model, iters, exp_name):
    check = {'state_dict':model.cpu().state_dict()}
    if '{}' in exp_name:
        exp_name = exp_name.format(iters)
    os.makedirs(os.path.dirname(exp_name), exist_ok=True)
    torch.save(check,exp_name)
    mlflow.log_artifact(exp_name)
    
    
def train(model, data, target, criterion, optimizer):
    optimizer.zero_grad()
    predict = model(data)
    loss = criterion(predict, target)
    loss.backward()
    optimizer.step()
    return float(loss.item())
    
    
def fit(model, device, train_set, dev_set,
        loss, optimizer, scheduler,
        validator=None, inferencer=None,
        check_name = '{}.model',
        n_epoch=10, i0=0, print_step=1, val_step=100,
        infer_step=100,check_step=1000):
        
    start = time.time()
    model.to(device)
    for i in range(n_epoch):
        losses = []
        for d, t in train_set:
            model.train()
            i0 += 1
            loss_val = train(model, d, t, loss, optimizer)
            scheduler.step()
            losses.append(loss_val)
            if i0 % print_step==0:
                avg_loss = sum(losses)/len(losses)
                mlflow.log_metric('loss', avg_loss)
                speed = int(print_step / (time.time() - start))
                start = time.time()
                print('Epoch {}; Speed: {}it/s; Iter {}; Loss: {:2.3f};'.format(
                    i, speed, i0, avg_loss))
                    
            if i0 % val_step==0 and validator is not None:
                model.eval()
                validator.start()
                with torch.no_grad():
                    for d, t in dev_set:
                        validator.step(model(d), t)
                validator.aggregate()
                
            if i0 % infer_step == 0 and inferencer is not None:
                model.eval()
                inferencer.inference(n_iter=i0)
                    
            if i0 % check_step==0: 
                save_check(model, i0, check_name)
                model.to(device)