import torch
import torchvision


class DetectionModel(torch.nn.Module):
    def __init__(self, size, mean, std,
                 model_type='fasterrcnn_mobilenet_v3_large_320',
                 ):
        """
        self.model is fasterrcnn_mobilenet_v3_large_320 by default. 3 channels input, 1 channel output
        """
        super().__init__()
        self.size = size
        if model_type=='fasterrcnn_mobilenet_v3_large_320':
            self.model = torchvision.models.detection.fasterrcnn_mobilenet_v3_large_320_fpn(num_classes=2)
            self.model.transform = torchvision.models.detection.transform.GeneralizedRCNNTransform(min_size=(size), max_size=size, image_mean = mean, image_std = std)
        else:
            print('Unknown model type!')
        

    def forward(self, x):
        """
        """
        data, target = x
        return self.model(data, target)
        
    def predict(self, x):
        """Predicts hats only for data"""
        return self.model(x)
        
