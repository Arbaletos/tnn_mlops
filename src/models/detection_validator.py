import matplotlib.pyplot as plt 

import os
import numpy as np
import torch

from src.data.image_utils import save_image

from torchvision.utils import draw_bounding_boxes

from sklearn.metrics import precision_recall_fscore_support

from mlflow import log_metric, log_artifact
    
def plot_boxes(img, boxes, figsize=8):
    """
    img is Tensor[CxWxH]
    boxes is array of [x1,y1,x2,y2] sequences
    """
    colors = ["yellow"]*len(boxes)
    result = draw_bounding_boxes((img*255).to(torch.uint8), boxes, colors=colors, width=2)
    plt.figure(figsize=(figsize,figsize)) 
    plt.imshow(result.permute(1,2,0).numpy())
    plt.show()
    

class DetValidator():
    def __init__(self, max_size=640):
        self.hats = []
        self.targets = []
        self.max_size = max_size
    
    def start(self):
        self.hats = []
        self.targets = []
    
    def step(self, hat, target):
        self.hats += hat
        self.targets += target
        
    def box_to_mask(self, boxes):
        ar = torch.zeros((self.max_size, self.max_size))
        for box in boxes:
            x1, y1, x2, y2 = [int(i.item()) for i in box]
            ar[x1:x2,y1:y2] = 1.
        return ar
        
    def aggregate(self):
        t_list = []
        h_list = []
        for tar, hat in zip(self.targets, self.hats):   
            t_list.append(self.box_to_mask(tar['boxes']).view(-1).cpu().numpy())
            h_list.append(self.box_to_mask(hat['boxes']).view(-1).cpu().numpy())
        t = np.concatenate(t_list)
        h = np.concatenate(h_list)
        p, r, f, s = precision_recall_fscore_support(np.concatenate(t_list),
                                                     np.concatenate(h_list))
        #print(self.hats)
        #print(self.targets)
        
        log_metric("Precision", p[1])
        log_metric("Recall", r[1])
        log_metric("F1-Score", f[1])
        print(f'Precision: {p[1]}')
        print(f'Recall: {r[1]}')
        print(f'F1-Score: {f[1]}')
        
class DetInferencer():
    def __init__(self, model=None,
                 infer_images = None,
                 log_dir = '',
                 preproc = None):
                 
        self.model = model
        self.infer_images = infer_images
        if infer_images is None:
            self.infer_images = []
        self.log_dir = log_dir
        self.preproc = preproc
        if preproc is None:
            self.preproc = lambda x: x
        
    def inference(self, n_iter=0):
        for i, img in enumerate(self.infer_images):
            t = self.preproc(img).unsqueeze(0)
            hat = self.model.predict(t)[0]
            shrink_ratio = max(t.shape[2:]) / self.model.size
            #print('Shrink:', shrink_ratio)
            #hat['boxes'] *= shrink_ratio
            boxes = hat['boxes']
            #print(f"Predict iter {n_iter}, boxes: {boxes}; shape: {t.shape}")
            colors = ["yellow"]*len(boxes)
            result = draw_bounding_boxes((t*255).squeeze(0).to(torch.uint8), boxes, colors=colors, width=4).permute(1,2,0).numpy()
            plt.figure(figsize=(8, 8)) 
            im_name = os.path.join(self.log_dir,f'iter_{n_iter}_image_{i}_predict.png')
            plt.imsave(im_name, result)
            log_artifact(im_name)
