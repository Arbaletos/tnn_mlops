import random
import os
import glob
import functools
import json
import numpy as np
import torch
from torch.utils.data import DataLoader
import torchvision.transforms as transforms

from src.models.detection_validator import DetValidator, DetInferencer
from src.data.image_utils import load_image, load_mask, split_image
from src.models.detection_model import DetectionModel
from src.models.train_utils import TorchDataset, fit
from src.markup.bbox import xywh_to_xyxy

import mlflow

    
def load_data(files):
    data = []
    target = []
    for f in files:
        if os.path.exists(f+'_bub.json') and os.path.exists(f+'_mask.png'):
            try:
                d = load_image(f+'_mask.png')
                bbox = json.loads(open(f+'_bub.json', 'r', encoding='utf-8').read())
                if set(bbox[0]) <= set(['x1', 'x2', 'y1', 'y2']):
                    t = [[box[i] for i in ['x1', 'x2', 'y1', 'y2']] for box in bbox]
                else:
                    t = [xywh_to_xyxy([box[i] for i in 'xywh']) for box in bbox]
            except Exception as e:
                print(f'Reading image exception, {f}, {e}')
                continue
            data.append(d)
            target.append(t)
    return data, target
    
    
class SquarePad:
    def __call__(self, tensor: torch.Tensor) -> torch.Tensor:
        c, w, h = tensor.shape
        max_dim = max(w,h)
        pad = torch.zeros((c, max_dim, max_dim))
        pad[:c, :w, :h] = tensor
        return pad
        
    def __repr__(self) -> str:
        return f"{self.__class__.__name__}"
    
    
def collate(data_list, preprocess, device):
    bs = len(data_list)
    data=[]
    target=[]
    for d, t in data_list:
        data.append(preprocess(d).unsqueeze(0))
        target.append({'labels': torch.tensor([1]*len(t), dtype=torch.long).to(device),
                       'boxes': torch.tensor(np.vstack(t)).to(device)})
    data = torch.cat(data).float().to(device)
    return (data, target), target
    
    
class ObjDetLoss():
    def __init__(self, weights = None):
        self.indices = ['loss_classifier', 'loss_box_reg', 'loss_objectness',  'loss_rpn_box_reg']
        if weights is None:
            self.weights ={k:1.0 for k in self.indices}
        elif type(weights is list):
            assert len(weights)==4
            self.weights ={k:v for k,v in zip(self.indices, weights)}
        elif type(weight is dict):
            assert len(weights)==4
            self.indices = list(weights.keys())
            self.weights ={k:weights[k] for k in weights}
        
    def __call__(self, hats, targets=None):
        """targets here are optional, all losses already in targets"""
        cum = sum([hats[i] for i in self.indices])
        return cum
        
mlflow.set_tracking_uri("sqlite:///mlflow.db")
tracking_uri = mlflow.get_tracking_uri()
 
mlflow.set_experiment(experiment_name = 'ObjDec_220614')
mlflow.set_tags({'task': 'Detection'})
    
print('Loading data...')
datadir = 'data/ready'
files = [g.replace('_bub.json', '') for g in glob.glob(os.path.join(datadir,'**/*_bub.json'),recursive=True)]
data, target = load_data(files)

data_size = len(data)
train_size = int(data_size*0.8)
print(f'Train_size: {train_size}, Dev size: {data_size-train_size}')

target_size = 320

exp_dir = 'models/ObjDec/exp/220614/'
img_dir = os.path.join(exp_dir, 'images')
os.makedirs(img_dir, exist_ok=True)

print('Calculating stats...')
cat = np.concatenate([d.reshape(-1,3) for d in data[:train_size]], axis=0)
m, s = cat.mean(axis=0), cat.std(axis=0)

preproc = transforms.Compose([
    transforms.ToTensor(),
    SquarePad(),
    transforms.Resize(size = [target_size, target_size])
])

preproc_solo = transforms.Compose([
    transforms.ToTensor(),
    SquarePad(),
])

print('Initializing model...')
torch.manual_seed(128)
device = torch.device('cpu')

model = DetectionModel(target_size, m, s)
validator = DetValidator()

inferencer = DetInferencer(model, infer_images = data[train_size:], 
                           log_dir = img_dir, preproc=preproc_solo)

optimizer = torch.optim.SGD(model.parameters(), lr=0.001, momentum=0.99)
scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=[100,1000,2000], gamma=0.5)

loss = ObjDetLoss()

bs = 1
train_set = DataLoader(TorchDataset(data[:train_size], target[:train_size]),
                       batch_size=bs, shuffle=True,
                       collate_fn=functools.partial(collate, preprocess=preproc, device=device))


dev_set = DataLoader(TorchDataset(data[train_size:], target[train_size:]),
                     batch_size=bs, shuffle=False,
                     collate_fn=functools.partial(collate, preprocess=preproc, device=device))

mlflow.log_params({'image_size':str(target_size),
                   'train_size':str(train_size),
                   'dev_size': str(data_size-train_size),
                   'seed': 128,
                   'lr': 0.001,
                   'scheduler': 'MultiStep(gamma=0.5, milestones=[100,1000,2000])',
                   'loss_classifier_weight': 0.25,
                   'loss_box_reg_weight':0.25,
                   'loss_objectness_weight':0.25,
                   'loss_rpn_box_reg_weight':0.25,
                   })
                   
print('Training...')
fit(model, device, train_set, dev_set, loss, optimizer, scheduler, validator, inferencer,
    check_name = os.path.join(exp_dir, '{}.model'),
    n_epoch = 1000, i0=0, print_step=1, val_step=10, check_step=100, infer_step=10)
