import os

import click
import imageio

from src.data.image_utils import load_image, load_mask, save_image

from src.click_utils import prompt_copy, ext_assert

def white_fill(img, mask):
    ret = img.copy()
    ret[mask > 0.0, :] = 1.0 #WHITISH WHITE
    return ret


@click.command()
@click.argument("in_img_path", type=click.Path(exists=True))
@click.argument("in_mask_path", type=click.Path(exists=True))
@click.argument("out_path", type=click.Path())
@click.option("-m", "--method", default="white_fill", help="method")
@click.option("-s", "--stub_path", default="", type=click.Path(), help="stub image")
def clean_image(
    in_img_path: str,
    in_mask_path: str,
    out_path: str,
    method: str = "stub",
    stub_path: str = "",
):
    """Cleanes (i.e. hides original text in bubbles) input image
    :param in_img_path: Path to read input image
    :param in_mask_path: Path to read input image
    :param out_path: Path to save binary mask
    :param method: method type to run (fill_white)
    :param stub_path: stub image for stub mode
    :return:
    """

    ext_assert(in_img_path, ".png")
    ext_assert(in_mask_path, ".png")
    ext_assert(out_path, ".png")
    if stub_path:
        if not os.path.exists(stub_path):
            print("Stub image is not found, autoresolving...")
            stub_path = in_img_path.replace("data.png", "clean.png")
            if not os.path.exists(stub_path) or not stub_path.endswith("clean.png"):
                print("Malsukcese!")
                return
            else:
                print(f"Stub image is: {stub_path}")
        ext_assert(stub_path, ".png")
        prompt_copy(stub_path, out_path)
        return
    
    img = load_image(in_img_path)
    mask = load_mask(in_mask_path)
    
    if method == 'white_fill':
        img_clean = white_fill(img, mask)
        save_image(out_path, img_clean)
        
    else:
        print("Not Implemented!")


if __name__ == "__main__":
    clean_image()
