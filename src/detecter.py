import os
import json
import itertools

import click

from src.click_utils import ext_assert, prompt_copy

@click.command()
@click.argument("in_path", type=click.Path(exists=True))
@click.argument("out_path", type=click.Path())
@click.option("-i", "--src_path", default="", type=click.Path(), help="source image")
@click.option("-s", "--stub_path", default="", type=click.Path(), help="stub image")
@click.option("-m", "--model", default="", help="model")
def detect_bubbles(
    in_path: str,
    out_path: str,
    src_path: str = "",
    model: str = "",
    stub_path: str = "",
):
    """Detects bubbles on the image and stores bubble-boxes in json file.
        In stub mode detects nothing and read bbox coordinates from png file.
    :param in_path: Path to read input binary mask
    :param out_path: Path to save bubble-box ids
    :param src_path: source image as an additional input
    :param stub_path: stub image for stub mode
    :param model: model type to run (e.g. ocv)
    :return:
    """
    ext_assert(in_path, ".png")
    ext_assert(out_path, ".json")

    if stub_path:
        ext_assert(stub_path, ".json")
        if not os.path.exists(stub_path):
            print("Stub json is not found, autoresolving...")
            stub_path = in_path.replace("mask.png", "bub.json")
            if not os.path.exists(stub_path):
                print("Malsukcese!")
                return
            else:
                print(f"Stub json is: {stub_path}")

        if stub_path != out_path:
            prompt_copy(stub_path, out_path)
        return
    print("Not Implemented!")


if __name__ == "__main__":
    detect_bubbles()
