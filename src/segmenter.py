import os

import click

from src.click_utils import prompt_copy, ext_assert
from src.data.image_utils import load_image, save_image
from src.models.eval_seg_model import init_seg_model, eval_seg_model


@click.command()
@click.argument("in_path", type=click.Path(exists=True))
@click.argument("out_path", type=click.Path())
@click.option("-m", "--model", type=click.Path(), default='', help="model_path")
@click.option("-p", "--preproc", type=click.Path(), default='')
@click.option("-d", "--device", default="cpu")
@click.option("-s", "--stub_path", default="", type=click.Path(), help="stub image")
def segment_image(
    in_path: str, out_path: str,
    model: str, preproc: str, device: str, stub_path: str
):
    """Creates binary bubble-mask of input strip
    :param in_path: Path to read input image
    :param out_path: Path to save binary mask
    :param model: model type to run (e.g. unet)
    :param stub_path: stub image for stub mode
    :return:
    """

    ext_assert(in_path, ".png")
    ext_assert(out_path, ".png")
    if stub_path:
        if not os.path.exists(stub_path):
            print("Stub image is not found, autoresolving...")
            stub_path = in_path.replace("data.png", "mask.png")
            if not os.path.exists(stub_path) or not stub_path.endswith("mask.png"):
                print("Malsukcese!")
                return
            else:
                print(f"Stub image is: {stub_path}")
        ext_assert(stub_path, ".png")
        prompt_copy(stub_path, out_path)
        return
    #try:
    img = load_image(in_path)
    model, preproc = init_seg_model(model, preproc, device)
    out = eval_seg_model(model, img, preproc, size=512, hop=512)
    save_image(out_path, out)
    #except Exception as e:
    #    print(e)


if __name__ == "__main__":
    segment_image()
